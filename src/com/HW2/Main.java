package com.HW2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by heinr on 25.09.2016.
 */
public class Main {

    public static void main(String[] args) throws Exception {

        System.out.println("Ich bin glück");

        int a = 3;
        int b = 7;
        int c = 1;

        int d = MinOfThree(a,b,c);

        System.out.println(d);

        a = 5;

        int e1 = FactFor(a);
        System.out.println(e1);

        int e2 = FactWhile(a);
        System.out.println(e2);

        int e3 = FactWhileDo(a);
        System.out.println(e3);

        Array10();

        Without5();

        Calc4();

        MinAndMaxOne();

        MinAndMaxTwo();

        UnderDiagonal();

        Friday();
    }

    public static int MinOfThree(int a, int b, int c) throws Exception {

        if (a > b) {
            if (a > c) {
                return a;
            } else {
                return c;
            }
        } else {
            if (b > c) {
                return b;
            } else {
                return c;
            }
        }
    }

    public static int FactFor(int a)throws Exception{

        int proizv = 1;

        for (int i = 1; i <= a; i++) {proizv = proizv * i;}

        return proizv;}

    public static int FactWhile(int a)throws Exception{

        int proizv = 1;
        int i = 1;

        while (i < a) { i++; proizv = proizv * i;}

        return proizv;}

    public static int FactWhileDo(int a)throws Exception{

        int proizv = 1;
        int i = 1;

        do{ i++; proizv = proizv * i;} while (i < a);

        return proizv;}

    public static void Array10()throws Exception{

        byte ArrByte[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};

        byte i = 0;

        while (i < 10) {
            System.out.println(ArrByte[i]);
            i++;}

    }

    public static void Without5()throws Exception{

        byte ArrByte[] = {1, 2, 5, 4, 5, 6, 7, 8, 9, 10, 11, 12, 5, 14, 15, 16, 17, 18, 19, 20};

        byte i = 0;

        while (i < ArrByte.length) {
            if (ArrByte[i] != 5)
            {System.out.println(ArrByte[i]);};
            i++;}

    }

    public static void Calc4()throws Exception{

        int a = 15;
        int b = 3;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String s = reader.readLine();


        if (s.length() == 1) {
        switch (s) {
            case "/":
                System.out.println("Result of divide operation is " + (a / b));
                break;
            case "*":
                System.out.println("Result of multiply operation is " + (a * b));
                break;
            case "+":
                System.out.println("Result of plus operation is " + (a + b));
                break;
            case "-":
                System.out.println("Result of minus operation is " + (a - b));
                break;
            default:
                System.out.println("I don't know what it is");
                break;
        }}


    }

    public static void MinAndMaxOne()throws Exception{

        byte ArrByte[] = {1, 2, 5, 4, 5, 6, 7, 8, -3, 9, 10, 11, 24, 12, 5, 14, 15, 16, 17, 18, 19, 20};

        byte SearchMin = ArrByte[0];
        byte SearchMax = ArrByte[0];
        byte i = 1;

        while (i < ArrByte.length) {
            if (ArrByte[i] > SearchMax) {SearchMax = ArrByte[i];};
            if (ArrByte[i] < SearchMin) {SearchMin = ArrByte[i];};
            i++;}

        System.out.println("Max. value is " + SearchMax + ", and Min. value is " + SearchMin);}

    public static void MinAndMaxTwo()throws Exception{

        byte ArrByte[][] = {{1, 2, 5, 4, 5, 6, 7, 8, -3, 9, 10, 11, 24, 12, 5, 14, 15, 16, 17, 18, 19, 20},
                {9, 2, 5, 4, 5, 6, 7, 8, -7, 9, 10, 11, 24, 12, 5, 14, 15, 16, 17, 18, 19, 2},
                {19, 12, 15, 4, 25, 6, 7, 8, -5, 9, 10, 11, 24, 12, 5, 14, 15, 16, 17, 18, 19, 12}};

        byte SearchMin = ArrByte[0][0];
        byte SearchMax = ArrByte[0][0];

        byte j = 0;

        while (j < ArrByte.length) {

            byte i = 0;

            while (i < ArrByte[j].length) {

                if (ArrByte[j][i] > SearchMax) {SearchMax = ArrByte[j][i];};
                if (ArrByte[j][i] < SearchMin) {SearchMin = ArrByte[j][i];};
                i++;}

            j++;}

        System.out.println("Max. value is " + SearchMax + ", and Min. value is " + SearchMin);

    }

    public static void UnderDiagonal()throws Exception{

        byte ArrByte[][] = {{1, 2, 5, 4, 5},
                {6, 7, 8, -3, 9},
                {0, 11, 24, 12, 5},
                {14, 15, 16, 17, 18},
                {9, 2, 5, 4, 4}};

        int Summa = 0;

        byte j = 0;

        while (j < ArrByte.length) {

            for (byte i = 0; i <= j; i++) {Summa = Summa + ArrByte[j][i];}

            j++;}

        System.out.println("Under diagonal Summa is " + Summa);

    }

    public static void Friday()throws Exception{

        byte ArrByte[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};

        byte SearchMin = ArrByte[0];
        byte SearchMax = ArrByte[0];

        byte i = 0;
        byte j = 0;

        while (i < ArrByte.length) {

            if (j == 4) {System.out.println("Friday is " + ArrByte[i]);
            }
            if (j == 7) {j = 0;}

            if (ArrByte[i] == 31) {
                switch (j) {
                    case 0:
                        System.out.println("Last day of month is Monday");
                        break;
                    case 1:
                        System.out.println("Last day of month is Tuesday");
                        break;
                    case 2:
                        System.out.println("Last day of month is Wednesday");
                        break;
                    case 3:
                        System.out.println("Last day of month is Thursday");
                        break;
                    case 4:
                        System.out.println("Last day of month is Friday");
                        break;
                    case 5:
                        System.out.println("Last day of month is Saturday");
                        break;
                    case 6:
                        System.out.println("Last day of month is Sunday");
                        break;
                    default:
                        System.out.println("I don't know what weekday is");
                        break;
                }
            }
            j++;
            i++;}

        System.out.println("Max. value is " + SearchMax + ", and Min. value is " + SearchMin);}

}
